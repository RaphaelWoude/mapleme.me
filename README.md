# 🍁Maple Memes🍁
## mapleme.me

The memes of the Maple community all in one place.

## How to add a brand new server:
1.  Fork this repository.
2.  Create a brand new file in the `servers` folder.
3.  Title it the name of the server, no spaces, all lowercased, and with the markdown file extension (`.md`).
4.  Enter your content in that file.
5.  Create a pull request.

## How to modify an existing page:
1.  Fork this repository.
2.  Make your desired changes to the file.
3.  Create a pull request.

Keep in mind that all images must use [Imgur](https://imgur.com/) direct links. If approved and merged, the changes will be made live shortly.
