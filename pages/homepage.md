# 🍁Maple Memes🍁

## The memes of the Maple community all in one place.

#### Want to contribute? [Submit a meme directly](/submit) or [create a pull request](https://bitbucket.org/SharpAceX/mapleme.me/src/master/). For questions or suggestions, message me on Discord at sharpacex#8359.

### Links

*   [Mushy Discord](https://discord.gg/9nv3GPQ)

### MEME SERVERS HALL OF FAME
*   [Omni](/servers/omni)
*   [Celestial MS](/servers/celestialms)
*   [Rexion o wait Cellion o wait Orybss](/servers/rexion)
*   [Old School Maple](/servers/oldschoolmaple)
*   [Maple Beta](/servers/maplebeta)
*   [Maple Clover](/servers/mapleclover)
*   [No Story](/servers/nostory)
*   [Lucky Story](/servers/luckystory)
*   [MapleGlobal](/servers/mapleglobal)
*   [Rien](/servers/rien)
*   [Khaini](/servers/khaini)
*   [RevolveMS](/servers/revolve)
*   [Elision](/servers/elision)
*   [Nostalria](/servers/nostalria)
*   [LucidMS](/servers/lucidms)

Want to add a new server here? [Submit your meme.](/submit)

### MAPLESTORY DEVELOPER TIER LIST

These are MapleStory developers, both past and present, ranked by tier.

*   S: sharpacex, zygon, mordred, moogra, PeterRabbit, yellow, snakeday, matze, koolk, hendi, haha01haha01, spudgy
*   A: arnah, shavit, eric, sysops, foxpat, diamondo, Angelspirit, S4nta, snow, OdinTeh, Lily, generic
*   B: Kaz, Holthelper, Roar, Charmander, feras, Maxcloud, Sjonnie, LaiLaiNoob, Xterminator, Senpai
*   C: Ronan, twdtwd, fraysa, sunnyboy, Darter, Oxysoft, rice, Aristocat, Osiris, buya
*   D: Aerodynamic, MechAviv, Izarooni, Chronos, Kevintjuh, poof, Willified, Warlock
*   E: Mazen, Wackyracker, Yuno, Sparks, RaptorJesus, Zydee, Suho, soulgirljp, Phantom
*   F: Snopboy, Resinate, ms, Angelic, ExtremeDevilz, Gerry, syntax-o, sporky

### CURRENT MEME SERVERS

In order of memery, these are current servers who are either endlessly in development with no ETA, or have an ETA that they're going to miss.

1.  Elision
2.  Maple Art Online
3.  Chirithy
4.  Orion

### Downloads

*   [Vertisy v90](/downloads/vertisy)
*   [Mushy Pro v179](/downloads/mushy-pro)
*   [Nox v176](/downloads/nox)
*   [Rien v83](/downloads/rien)
*   [Evan Client v83](/downloads/evan-client)

People don't like the taste of their own medicine lol

*   [Page 1](https://web.archive.org/web/20171226221848/http://forum.ragezone.com/f427/merry-christmas-2017-vertisy-v90-1142671/)
*   [Page 2](https://web.archive.org/web/20171226221814/http://forum.ragezone.com/f427/merry-christmas-2017-vertisy-v90-1142671/index2.html)

![](https://media0.giphy.com/media/39iLJYCGBB8ZrXyyM5/giphy.gif)  
![](https://i.imgur.com/U3PH3Me.jpg)  
![](https://i.gyazo.com/5c5eda8d8385c02ca849b59a47d564a9.png)  
![](https://i.gyazo.com/46b6c5e777edd3c1fbdabad6e9ee748f.png)  
![](https://i.gyazo.com/af533c4a4aa89b3d635891685549fd9e.png)  
![](https://i.gyazo.com/dd6740b97aa58a917acc6f8c97a537d9.png)

<iframe width="900" height="400" src="https://www.youtube.com/embed/ptBkUV9ej4M?autoplay=1&amp;loop=1&amp;playlist=ptBkUV9ej4M" frameborder="0" allowfullscreen=""></iframe>  

![](https://i.imgur.com/DyXfmQA.png)
![](https://image.ibb.co/jdtXn6/0e1.gif)
